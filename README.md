## MasterMind

MasterMind jest adaptacją gry logiczej o tym samym tytule - [wikipedia](https://pl.wikipedia.org/wiki/Mastermind_(gra_planszowa))
Jest to jedna z moich pierwszych w życiu aplikacji konsolowych napisanych w Javie.

- Spis treści:

1. O aplikacji
2. Jak grać
3. Jak wygrać
4. Linki do pozostałych projektów

#### O aplikacji:

MasterMind to aplikacja treningowa napisana z myśla o przećwiczeniu i opanowaniu instrukcji warunkowych,
operatorów logicznych i matematycznych pętli i tablic.

Jak wspomniałem wyżej, aplikacja ta jest przeniesieniem gry logicznej, w której 'komputer' wciela się w rolę
szyfrującego i podpowiadającego, a my gracze musimy odgadnąć hasło, które jest cztero cyfrowe, gdzie każdy element mieści
się w przedziale od 1 do 6.

Dodatkowo gra jest zabezpieczona przed błędani, dzięki czemu aplikacja nie zawiesza się, licznik rund zostaje poprawny, a 
na ekranie pojawia się odpowiedni komunikat.

Ostatnią rzeczą jaką napisałem, jest przesyłanie rozgrywki i błędów do osobnych logów:

- przykład logów z rozgrywki:

````
##### NOWA GRA ####
01-10-2017 04:37:47  - - - -  Szukany kod to: 	[4, 1, 6, 3]
01-10-2017 04:37:47  - - - -  Poziom trudności gry: 	[32mNORMAL[0m (12 rund)
01-10-2017 04:37:47  - - - -  Runda: 1	[1, 2, 3, 4]	T:0, Z: 3, P: 1
01-10-2017 04:37:47  - - - -  Runda: 2	[4, 3, 2, 1]	T:1, Z: 2, P: 1
01-10-2017 04:37:47  - - - -  Runda: 3	[4, 4, 2, 2]	T:1, Z: 0, P: 3
01-10-2017 04:37:47  - - - -  Runda: 4	[4, 2, 1, 2]	T:1, Z: 1, P: 2
01-10-2017 04:37:47  - - - -  Runda: 5	[4, 6, 5, 3]	T:2, Z: 1, P: 1
01-10-2017 04:37:47  - - - -  Runda: 6	[4, 2, 2, 3]	T:2, Z: 0, P: 2
01-10-2017 04:37:47  - - - -  Runda: 7	[4, 4, 5, 3]	T:2, Z: 0, P: 2
01-10-2017 04:37:47  - - - -  Runda: 8	[4, 1, 1, 3]	T:3, Z: 0, P: 1
01-10-2017 04:37:47  - - - -  Runda: 9	[4, 1, 4, 3]	T:3, Z: 0, P: 1
01-10-2017 04:37:47  - - - -  Runda: 10	[4, 1, 2, 3]	T:3, Z: 0, P: 1
01-10-2017 04:37:47  - - - -  Runda: 11	[4, 1, 3, 3]	T:3, Z: 0, P: 1
01-10-2017 04:37:47  - - - -  Runda: 12	[4, 1, 6, 3]	T:4, Z: 0, P: 0
01-10-2017 04:37:47  - - - -  KONIEC GRY - WYGRANA

````

#### Jak grać:

- Gra poprosi nas o ustalenie poziomu trudności (ilośc rund)

[![Diff](https://i.imgur.com/38KW2dE.png)](https://i.imgur.com/38KW2dE.png)
- Przechodzimy do zgadywania szyfru*

[![Guess](https://i.imgur.com/FH0c1GB.png)](https://i.imgur.com/FH0c1GB.png)

Szyfr zawsze składa się z 4 cyfr, gdzie pojedyncza cyfra jest z przedziału od 1 do 6, czyli"
- bedzie z przedzialu od 1111 do 6666 z wylaczeniem zer. 
- Przyklady poprawnego kodu: 1234, 6545, 1352, 4433.
- Przyklady blednego kodu: 0124, 1987, 0909, 1667.


Uwaga: podanie błędnego kodu spowoduje wyświetlenie komunikatu o błędzie, aplkacja nie zawiesi się,
licznik rund nie zostanie zmieniony.

#### Jak wygrac?:

Po podaniu prawidlowego kodu gra przekaże nam podpowiedzi dot. szyfru.

- Kolor zielony wskazuje ile cyfr zgadliśmy i znajdują sie na swoim miejscu.
- Kolor żółty wskazuje ile cyfr zgadliśmy, ale nie znajdują sie na swoim miejscu.
- Kolor czerwony wskazuje ile cyfr nie znajduje się w szyfrze

[![Example](https://i.imgur.com/LDRcO3y.png)](https://i.imgur.com/LDRcO3y.png)

#### Moje opublikowane projekty
1. [Simple RPG Game](https://gitlab.com/jjerzynski/SimpleRpgGame) - (w produkcji) Okienkowa gra RPG stylizowana na produkcje z lat '80-'90, mój największy projekt;
2. [Weather App](https://gitlab.com/jjerzynski/SimpleWeatherApp) - (projekt zakończony) Konsolowa aplikacja pogodowa oparna na REST;
3. [Weather App 2](https://gitlab.com/jjerzynski/RESTWeatherApp) - (w produkcji) Ulepszona aplikacja pogodowa w oparciu o REST i JSON oraz Swinga;
4. [Mastermind](https://gitlab.com/jjerzynski/MasterMindConsole) - (projekt zakończony) Gra logiczna z lat '80;
5. [TicTacToe](https://gitlab.com/jjerzynski/TicTacToeGame) - (projekt zakończony) Kóło i krzyżyk - projekt na zaliczenie zajęc w Craftin' Code, mój pierwszy projekt;
