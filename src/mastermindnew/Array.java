package mastermindnew;

import java.util.Random;

/**
 * @author Jakub Jerzyński
 */
class Array {

    int[] numberArray;

    Array() {
        numberArray = new int[4];
    }

    protected void showArray() {
        for (int i = 0; i < numberArray.length; i++) {
            System.out.print(numberArray[i] + " ");
        }
    }

    protected int[] getArray() {
        return new int[]{numberArray[0], numberArray[1], numberArray[2], numberArray[3]};
    }
}

class GuessArray extends Array {

    private final String NUMBER_TO_STRING;

    GuessArray(String number) {
        super();
        NUMBER_TO_STRING = number;
        guessToArray();
    }

    private void guessToArray() {
        char[] charTempArray = NUMBER_TO_STRING.toCharArray();
        numberArray = new int[4];

        for (int i = 0; i < charTempArray.length; i++) {
            numberArray[i] = (int) charTempArray[i] - 48;
        }
    }

    protected boolean checkAnswer() {
        boolean answerIs = true;

        for (int i = 0; i < numberArray.length; i++) {
            if (numberArray[i] < 1 || numberArray[i] > 6) {
                throw new ArrayIndexOutOfBoundsException("Na pozycji: " + (i+1) + ", podano znak " + Integer.toString(numberArray[i]) + ", który wykracza poz dozw. zakres");
            }
            answerIs = false;
        }
        return answerIs;
    }
}

class AnswerArray extends Array {

    AnswerArray() {
        super();
    }

    protected void fillNumberArray() {
        Random genNumber = new Random();

        for (int i = 0; i < numberArray.length; i++) {
            numberArray[i] = genNumber.nextInt(6 - 1 + 1) + 1;
        }
    }
}
