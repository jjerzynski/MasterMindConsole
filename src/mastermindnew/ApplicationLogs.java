package mastermindnew;

import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import static mastermindnew.MasterMindNewMain.DEF_COLOUR;
import static mastermindnew.MasterMindNewMain.RED_COLOUR;

/**
 * @author Jakub Jerzyński
 */
class ApplicationLogs {

    private final String DATE = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
    private final String PRECISE_DATE = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date());

    protected void errorLog(String excText) {
        String fileName = "resources\\logs\\errorlogs\\" + DATE + " errorLog.txt";

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, true))){
            
                bw.write("\n\r" + PRECISE_DATE + "  - - -  " + excText + "\r");
                bw.flush();
                
        } catch (IOException exc) {
            System.out.println(RED_COLOUR + "AppError! " + DEF_COLOUR + "Nie mogę wysłać błędu do logów, błąd nie zostanie zapisany");
        }
    }

    protected void gameLog(String moveText) {
        String fileName = "resources\\logs\\gamelogs\\" + DATE + " gameLog.txt";

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, true))){

                bw.write("\n\r" + PRECISE_DATE + "  - - - -  " + moveText + "\r");
                bw.flush();
                
        } catch (IOException exc) {
            System.out.println(RED_COLOUR + "AppError! " + DEF_COLOUR + "Nie mogę wysłać wprowadzonej opcji do logów! Logi nie zostaną zapisane!");
        }
    }
}
