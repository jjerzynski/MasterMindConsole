package mastermindnew;

/**
 * @author Jakub Jerzyński
 */
class Rounds {

    private int noOfRounds;

    Rounds() {
        this.noOfRounds = 12;
    }

    Rounds(int noOfRounds) {
        this.noOfRounds = noOfRounds;
    }

    protected void setNoOfRounds(int noOfRounds) {
        this.noOfRounds = noOfRounds;
    }

    protected int getNoOfRounds() {
        return noOfRounds;
    }

}
