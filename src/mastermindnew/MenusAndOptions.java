package mastermindnew;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import static mastermindnew.MasterMindNewMain.DEF_COLOUR;
import static mastermindnew.MasterMindNewMain.GREEN_COLOUR;
import static mastermindnew.MasterMindNewMain.RED_COLOUR;
import static mastermindnew.MasterMindNewMain.YELLOW_COLOUR;
import static mastermindnew.MasterMindNewMain.numberOfRounds;

/**
 * @author Jakub Jerzyński
 */
class MenusAndOptions {

    ApplicationLogs LOGS = new ApplicationLogs();

    protected void getDefaultMenu() {
        System.out.println("1. dalej, 0. zakończ program");
    }

    protected void logo() {
        String logo;
        String fileName = "resources\\logo.txt";

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            System.out.println();
            while ((logo = br.readLine()) != null) {
                System.out.println(GREEN_COLOUR + logo + DEF_COLOUR);
            }
            br.close();
        } catch (IOException exc) {
            LOGS.errorLog("Błąd wczytywania pliku:\t" + exc.toString());
            System.out.println(RED_COLOUR + "Błąd odczytu pliku logo.txt:" + DEF_COLOUR + " " + exc);
        }
    }

    protected void mainMenu() {
        String menuText;
        String fileName = "resources\\menutext.txt";

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            System.out.println();
            while ((menuText = br.readLine()) != null) {
                System.out.println(menuText);
            }
            br.close();
        } catch (IOException exc) {
            LOGS.errorLog("Błąd wczytywania pliku:\t " + exc.toString());
            System.out.println(RED_COLOUR + "Błąd odczytu pliku menutext.txt:" + DEF_COLOUR + " " + exc);
            System.out.print(YELLOW_COLOUR + "(Menu awaryjne)" + DEF_COLOUR + "1 - Rozpocznij grę, 3 - zakończ program" + "\n");
        }
    }

    protected void executeMainMenuChoice(int mainMenuChoice) throws IOException, InterruptedException {

        switch (mainMenuChoice) {
            case 1:
                MasterMindNewMain.prepereGame();
                break;
            case 2:
                howToPlay();
                break;
            case 3:
                System.out.println(RED_COLOUR + "PROGRAM ZAKOŃCZYŁ DZIAŁANIE" + DEF_COLOUR);
                System.exit(0);
                break;
            default:
                MasterMindNewMain.startMasterMindGame();
                break;
        }
    }

    protected void difficultyMenu() {
        String difficultyText;
        String fileName = "resources\\difficultytext.txt";

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            System.out.println();
            while ((difficultyText = br.readLine()) != null) {
                System.out.println(difficultyText);
            }
            br.close();
        } catch (IOException exc) {
            LOGS.errorLog("Błąd wczytywania pliku:\t " + exc.toString());
            System.out.println(RED_COLOUR + "Błąd odczytu pliku difficultytext.txt:" + DEF_COLOUR + " " + exc);
            System.out.print(YELLOW_COLOUR + "(Menu awaryjne)" + DEF_COLOUR + "1 - 16 rund, 2 - 12 rund, 3 - 10 rund, 4 - 8 rund" + "\n");
        }
    }

    protected int executeDifficultyChoice(int difficultyChoice) throws IOException, InterruptedException {

        switch (difficultyChoice) {
            case 1:
                numberOfRounds = 16;
                MasterMindNewMain.difficultyName = GREEN_COLOUR + "EASY" + DEF_COLOUR;
                break;
            case 2:
                numberOfRounds = 12;
                MasterMindNewMain.difficultyName = GREEN_COLOUR + "NORMAL" + DEF_COLOUR;
                break;
            case 3:
                numberOfRounds = 10;
                MasterMindNewMain.difficultyName = YELLOW_COLOUR + "HARD" + DEF_COLOUR;
                break;
            case 4:
                numberOfRounds = 8;
                MasterMindNewMain.difficultyName = RED_COLOUR + "VERY HARD" + DEF_COLOUR;
                break;
            default:
                MasterMindNewMain.setDifficulty();
                break;
        }
        return numberOfRounds;
    }

    protected void howToPlay() throws IOException, InterruptedException {
        String rules;
        String fileName = "resources\\howtoplay.txt";

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            System.out.println();
            while ((rules = br.readLine()) != null) {
                System.out.println(rules);
            }
            br.close();
        } catch (IOException exc) {
            LOGS.errorLog("Błąd wczytywania pliku:\t" + exc.toString());
            System.out.println(RED_COLOUR + "Błąd odczytu pliku howtoplay.txt:" + DEF_COLOUR + " " + exc);
        }

        howToPlayMenu();
    }

    protected void howToPlayMenu() throws IOException, InterruptedException {
        String howToPlayMenuText;
        String fileName = "resources\\howtoplaymenutext.txt";

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            System.out.println();
            while ((howToPlayMenuText = br.readLine()) != null) {
                System.out.println(howToPlayMenuText);
            }
            br.close();
        } catch (IOException exc) {
            LOGS.errorLog("Błąd wczytywania pliku:\t " + exc.toString());
            System.out.println(RED_COLOUR + "Błąd odczytu pliku howtoplaymenutext.txt:" + DEF_COLOUR + " " + exc);
            System.out.print(YELLOW_COLOUR + "(Menu awaryjne)" + DEF_COLOUR + "1 - Menu Główne, 2 - Gra Testowa" + "\n");
        }
        MasterMindNewMain.howToPlayChoice();
    }

    protected void executeHowToPlayMenuChoice(int howToPlayMenuChoice) throws IOException, InterruptedException {
        switch (howToPlayMenuChoice) {
            case 1:
                MasterMindNewMain.startMasterMindGame();
                break;
            case 2:
                MasterMindNewMain.prepereTestGame();
                break;
            default:
                howToPlayMenu();
                break;
        }
    }

    protected void endGameMenu() {
        String endGameMenuText;
        String fileName = "resources\\endgamemenutext.txt";

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            System.out.println();
            while ((endGameMenuText = br.readLine()) != null) {
                System.out.println(endGameMenuText);
            }
            br.close();
        } catch (IOException exc) {
            LOGS.errorLog("Błąd wczytywania pliku:\t " + exc.toString());
            System.out.println(RED_COLOUR + "Błąd odczytu pliku endgamemenutext.txt:" + DEF_COLOUR + " " + exc);
            System.out.print(YELLOW_COLOUR + "(Menu awaryjne)" + DEF_COLOUR + "1 - Rozpocznij grę, 3 - zakończ program" + "\n");
        }
    }

    protected void executeEndGameMenuChoice(int endGameMenuChoice) throws IOException, InterruptedException {

        switch (endGameMenuChoice) {
            case 1:
                MasterMindNewMain.startMasterMindGame();
                break;
            case 2:
                logo();
                MasterMindNewMain.clearAnswerCalculates();
                MasterMindNewMain.prepereGame();
                break;
            case 3:
                System.out.println(RED_COLOUR + "PROGRAM ZAKOŃCZYŁ DZIAŁANIE" + DEF_COLOUR);
                System.exit(0);
                break;
            default:
                endGameMenu();
                break;
        }
    }

    protected void endTestGameMenu() {
        String endTestGameMenuText;
        String fileName = "resources\\endtestgamemenutext.txt";

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            System.out.println();
            while ((endTestGameMenuText = br.readLine()) != null) {
                System.out.println(endTestGameMenuText);
            }
            br.close();
        } catch (IOException exc) {
            LOGS.errorLog("Błąd wczytywania pliku:\t " + exc.toString());
            System.out.println(RED_COLOUR + "Błąd odczytu pliku endgamemenutext.txt:" + DEF_COLOUR + " " + exc);
            System.out.print(YELLOW_COLOUR + "(Menu awaryjne)" + DEF_COLOUR + "1 - Rozpocznij grę, 3 - zakończ program" + "\n");
        }
    }

    protected void executeEndTestGameMenuChoice(int testGameEndsChoice) throws IOException, InterruptedException {
        switch (testGameEndsChoice) {
            case 1:
                MasterMindNewMain.startMasterMindGame();
                break;
            case 2:
                logo();
                MasterMindNewMain.clearAnswerCalculates();
                MasterMindNewMain.prepereTestGame();
                break;
            case 3:
                howToPlay();
                break;
            default:
                endTestGameMenu();
                break;
        }
    }
}
