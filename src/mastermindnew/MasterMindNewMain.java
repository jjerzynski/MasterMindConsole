package mastermindnew;

import java.io.IOException;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author Jakub Jerzyński
 */
public class MasterMindNewMain {


    protected static int[] whitePinArrayFromAnswer = new int[6];
    protected static int[] whitePinArrayFromGuess = new int[6];

    protected static int blackPins;
    protected static int whitePins;
    protected static int notBlackPins;
    protected static int maxRounds;
    protected static int numberOfRounds;
    protected static int roundCounter = 0;

    private static final MenusAndOptions MENUS = new MenusAndOptions();
    private static final Rounds NUMBER_OF_ROUNDS = new Rounds();
    private static final AnswerArray ANSWER = new AnswerArray();
    private static final ApplicationLogs LOGS = new ApplicationLogs();

    protected static final String DEF_COLOUR = "\u001B[0m";
    protected static final String RED_COLOUR = "\u001B[31m";
    protected static final String GREEN_COLOUR = "\u001B[32m";
    protected static final String BLUE_COLOUR = "\u001B[34m";
    protected static final String YELLOW_COLOUR = "\u001B[33m";

    protected static String difficultyName;

    public static void main(String[] args) throws IOException, InterruptedException {

        startMasterMindGame();

    }

    protected static void startMasterMindGame() throws IOException, InterruptedException {
        int mainMenuChoice;

        screenCleaner();
        MENUS.logo();
        MENUS.mainMenu();

        try {
            System.out.print("Wybierz odpowiednia opcje i nacisnij [Enter]: ");
            mainMenuChoice = new Scanner(System.in).nextInt();
            MENUS.executeMainMenuChoice(mainMenuChoice);
        } catch (InputMismatchException exc) {
            LOGS.errorLog("Main menu: \t" + exc.toString());
            MENUS.executeMainMenuChoice(0);
        }
    }


    protected static void howToPlayChoice() throws IOException, InterruptedException {
        int howToPlayMenuChoice;

        try {
            System.out.print("Wybierz odpowiednia opcje i nacisnij [Enter]: ");
            howToPlayMenuChoice = new Scanner(System.in).nextInt();
            MENUS.executeHowToPlayMenuChoice(howToPlayMenuChoice);
        } catch (InputMismatchException exc) {
            LOGS.errorLog("Main menu: \t" + exc.toString());
            MENUS.executeHowToPlayMenuChoice(0);
        }
    }

    protected static void prepereGame() throws IOException, InterruptedException {

        if (roundCounter > 0) {
            resetRoundCounter();
        }
        ANSWER.fillNumberArray();
        LOGS.gameLog("\n" + "##### NOWA GRA ####");
        LOGS.gameLog("Szukany kod to: \t" + Arrays.toString(ANSWER.getArray()));
        setDifficulty();
    }

    protected static void setDifficulty() throws IOException, InterruptedException {
        int difficultyChoice;

        screenCleaner();
        MENUS.logo();
        MENUS.difficultyMenu();

        try {
            System.out.print("\n" + "Wybierz poziom trudności i naciśnij [Enter]: ");
            difficultyChoice = new Scanner(System.in).nextInt();
            MENUS.executeDifficultyChoice(difficultyChoice);
        } catch (InputMismatchException exc) {
            LOGS.errorLog("Difficulty menu: \t " + exc.toString());
            MENUS.executeDifficultyChoice(0);
        }

        NUMBER_OF_ROUNDS.setNoOfRounds(numberOfRounds);
        MasterMindNewMain.playGame();
    }

    protected static void playGame() throws IOException, InterruptedException {
        screenCleaner();
        MENUS.logo();
        System.out.println("Wybrany poziom trudności to " + difficultyName + " (" + NUMBER_OF_ROUNDS.getNoOfRounds() + " rund)");
        LOGS.gameLog("Poziom trudności gry: \t" + difficultyName + " (" + NUMBER_OF_ROUNDS.getNoOfRounds() + " rund)");

        for (;;) {
            try {
                System.out.print("Runda: " + (roundCounter + 1) + ", Podaj kod: ");
                String playerNumber = new Scanner(System.in).nextLine();
                GuessArray guess = new GuessArray(playerNumber);
                setBlackPins(ANSWER.getArray(), guess.getArray());
                prepereWhitePins(ANSWER.getArray(), guess.getArray());
                guess.checkAnswer();
                System.out.println("T: " + GREEN_COLOUR + getBlackPins() + DEF_COLOUR
                        + ", Z: " + YELLOW_COLOUR + getWhitePins(ANSWER.getArray(), guess.getArray()) + DEF_COLOUR
                        + ", P: " + RED_COLOUR + getMissedPins() + DEF_COLOUR);
                LOGS.gameLog("Runda: " + (roundCounter + 1) + "\t" + Arrays.toString(guess.getArray()) + "\t"
                        + "T:" + getBlackPins() + ", Z: " + getWhitePins(ANSWER.getArray(), guess.getArray()) + ", P: " + getMissedPins());
            } catch (ArrayIndexOutOfBoundsException exc) {
                LOGS.errorLog("Gra - Podanie kodu: \t" + exc);
                System.out.print(RED_COLOUR + "BŁĄD!" + DEF_COLOUR + " Podałeś za długi lub błędny kod" + "\n");
                roundCounter--;
            }
            roundCounter++;
            if (chceckWinCondition() == true) {
                victory();
                break;
            } else if (roundCounter >= NUMBER_OF_ROUNDS.getNoOfRounds()) {
                defeat();
                break;
            }
        }
    }

    protected static boolean chceckWinCondition() {
        boolean win = blackPins == 4 ? true : false;
        return win;
    }

    protected static void victory() throws IOException, InterruptedException {
        int victoryChoice;
        System.out.println("\n" + GREEN_COLOUR + "Gratulacje wygrałeś" + DEF_COLOUR);
        System.out.println("Szukany kod to " + Arrays.toString(ANSWER.getArray()));
        System.out.println("Ilość rozegranych rund: " + roundCounter);

        LOGS.gameLog("KONIEC GRY - WYGRANA");
        MENUS.endGameMenu();

        try {
            System.out.print("Wybierz odpowiednia opcje i nacisnij [Enter]: ");
            victoryChoice = new Scanner(System.in).nextInt();
            MENUS.executeEndGameMenuChoice(victoryChoice);
        } catch (InputMismatchException exc) {
            LOGS.errorLog("Victory Menu: \t" + exc.toString());
            MENUS.executeEndGameMenuChoice(0);
        }
    }

    protected static void defeat() throws IOException, InterruptedException {
        int defeatChoice;
        System.out.println("\n" + RED_COLOUR + "Niestety przegrałeś" + DEF_COLOUR);
        System.out.println("Szukany kod to " + Arrays.toString(ANSWER.getArray()));
        System.out.println("Ilość rozegranych rund: " + roundCounter);

        LOGS.gameLog("KONIEC GRY - PRZEGRANA");
        MENUS.endGameMenu();

        try {
            System.out.print("Wybierz odpowiednia opcje i nacisnij [Enter]: ");
            defeatChoice = new Scanner(System.in).nextInt();
            MENUS.executeEndGameMenuChoice(defeatChoice);
        } catch (InputMismatchException exc) {
            LOGS.errorLog("Defeat Menu: \t" + exc.toString());
            MENUS.executeEndGameMenuChoice(0);
        }
    }

    protected static void prepereTestGame() throws IOException, InterruptedException {

        screenCleaner();
        MENUS.logo();
        clearAnswerCalculates();
        resetRoundCounter();

        ANSWER.fillNumberArray();

        LOGS.gameLog("\n\r" + "#### GRA TESTOWA ####");
        LOGS.gameLog("Poziom trudności gry: TEST (ilość rund: 8)");
        LOGS.gameLog("Szukany kod to: \t" + Arrays.toString(ANSWER.getArray()));

        System.out.println("\n" + YELLOW_COLOUR + "GRA TESTOWA" + DEF_COLOUR);
        System.out.println("Wybrany poziom trudności to " + GREEN_COLOUR + "TEST" + DEF_COLOUR + " (ilość rund: 8)");
        System.out.println("Przetestuj sobie, jak działa gra. Masz na to 8 rund" + ""
                + "\n" + "Jeżeli jednak chcesz zakończyć wcześniej, wpisz szukany kod!");
        System.out.println("Szukany kod to: " + BLUE_COLOUR + Arrays.toString(ANSWER.getArray()) + DEF_COLOUR + "\n");

        playTestGame();

    }

    protected static void playTestGame() throws IOException, InterruptedException {

        for (;;) {
            try {
                System.out.print("Runda: " + (roundCounter + 1) + ", Podaj kod: ");
                String playerNumber = new Scanner(System.in).nextLine();
                GuessArray guess = new GuessArray(playerNumber);
                setBlackPins(ANSWER.getArray(), guess.getArray());
                prepereWhitePins(ANSWER.getArray(), guess.getArray());
                guess.checkAnswer();
                System.out.println("T: " + GREEN_COLOUR + getBlackPins() + DEF_COLOUR
                        + ", Z: " + YELLOW_COLOUR + getWhitePins(ANSWER.getArray(), guess.getArray()) + DEF_COLOUR
                        + ", P: " + RED_COLOUR + getMissedPins() + DEF_COLOUR);
                LOGS.gameLog("Runda: " + (roundCounter + 1) + "\t" + Arrays.toString(guess.getArray()) + "\t"
                        + "T:" + getBlackPins() + ", Z: " + getWhitePins(ANSWER.getArray(), guess.getArray()) + ", P: " + getMissedPins());
            } catch (ArrayIndexOutOfBoundsException exc) {
                LOGS.errorLog("Gra - Podanie kodu: \t" + exc);
                System.out.print(RED_COLOUR + "BŁĄD!" + DEF_COLOUR + " Podałeś za długi lub błędny kod");
                roundCounter--;
            }
            roundCounter++;
            if (chceckWinCondition() == true) {
                testGameEnds();
                break;
            } else if (roundCounter >= NUMBER_OF_ROUNDS.getNoOfRounds()) {
                testGameEnds();
                break;
            }
        }
    }

    protected static void testGameEnds() throws IOException, InterruptedException {
        int testGameEndsChoice;
        System.out.println("\n" + GREEN_COLOUR + "Gratulacje skończyłeś grę testową!" + DEF_COLOUR);
        System.out.println("Ilość rozegranych rund: " + roundCounter);

        LOGS.gameLog("KONIEC GRY TESTOWEJ");
        MENUS.endTestGameMenu();

        try {
            System.out.print("Wybierz odpowiednia opcje i nacisnij [Enter]: ");
            testGameEndsChoice = new Scanner(System.in).nextInt();
            MENUS.executeEndTestGameMenuChoice(testGameEndsChoice);
        } catch (InputMismatchException exc) {
            LOGS.errorLog("Victory Menu: \t" + exc.toString());
            MENUS.executeEndTestGameMenuChoice(0);
        }
    }

    protected static void setBlackPins(int[] answer, int[] guess) {
        clearAnswerCalculates();

        for (int i = 0; i < 4; i++) {
            if (answer[i] == guess[i]) {
                blackPins++;
            } else if (answer[i] != guess[i]) {
                notBlackPins++;
            }
        }
        chceckWinCondition();
    }

    protected static void clearAnswerCalculates() {
        blackPins = 0;
        notBlackPins = 0;

        for (int i = 0; i < 6; i++) {
            whitePinArrayFromAnswer[i] = 0;
            whitePinArrayFromGuess[i] = 0;
        }
    }

    protected static void resetRoundCounter() {
        roundCounter = 0;
    }

    protected static void prepereWhitePins(int[] answer, int[] guess) {
        final int RESULT = 1;

        for (int i = 0; i < 4; i++) {
            for (int j = 1; j <= 6; j++) {
                if (answer[i] == j) {
                    whitePinArrayFromAnswer[j - 1] += RESULT;
                }
                if (guess[i] == j) {
                    whitePinArrayFromGuess[j - 1] += RESULT;
                }
            }
        }
    }

    protected static int getWhitePins(int[] answer, int[] guess) {
        whitePins = 0;
        whitePins = notBlackPins;

        for (int i = 0; i < 6; i++) {
            if (whitePinArrayFromAnswer[i] < whitePinArrayFromGuess[i]) {
                whitePins = whitePins - (whitePinArrayFromGuess[i] - whitePinArrayFromAnswer[i]);
            }
        }
        return whitePins;
    }

    protected static int getBlackPins() {
        return blackPins;
    }

    protected static int getMissedPins() {
        return 4 - blackPins - whitePins;
    }

    protected static void screenCleaner() throws IOException, InterruptedException {
        new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
    }

}
